import React, { Component } from 'react'

export class Calculator extends Component {
  constructor(props) {
    super(props)

    this.state = {
         result: ''
    }
  }

  onClick = value => {
    if(value === '=') {
        this.calculate()
    }
    else if(value === 'C'){
        this.remove()
    }
    else if(value === 'CE'){
        this.reset()
    }
    else{
      this.setState(prevState => ({
        result : prevState.result + value
      }))
    }
  }

  calculate = () => {
    try {
      this.setState(prevState => ({
        result : eval(prevState.result)
      }))
    }
    catch {
      this.setState({
        result: 'error'
      })
    }
  }

  reset = () => {
    this.setState({
      result: ''
    })
  }

  remove = () => {
    this.setState(prevState => ({
      result : prevState.result.slice(0, -1)
    }))
  }

  render() {
    return (
      <div>
        <textarea value = {this.state.result} rows = '5'columns = '100'/>
        <div>
          <div>
            <button onClick ={(event) => this.onClick('(')}>(</button>
            <button onClick ={(event) => this.onClick('CE')}>CE</button>
            <button onClick ={(event) => this.onClick('C')}>C</button>
            <button onClick ={(event) => this.onClick(')')}>)</button>
          </div>
          <div>
            <button onClick ={(event) => this.onClick('1')}>1</button>
            <button onClick ={(event) => this.onClick('2')}>2</button>
            <button onClick ={(event) => this.onClick('3')}>3</button>
            <button onClick ={(event) => this.onClick('+')}>+</button>
          </div>
          <div>
            <button onClick ={(event) => this.onClick('4')}>4</button>
            <button onClick ={(event) => this.onClick('5')}>5</button>
            <button onClick ={(event) => this.onClick('6')}>6</button>
            <button onClick ={(event) => this.onClick('-')}>-</button>
          </div>
          <div>
            <button onClick ={(event) => this.onClick('7')}>7</button>
            <button onClick ={(event) => this.onClick('8')}>8</button>
            <button onClick ={(event) => this.onClick('9')}>9</button>
            <button onClick ={(event) => this.onClick('*')}>*</button>
          </div>
          <div>
            <button onClick ={(event) => this.onClick('.')}>.</button>
            <button onClick ={(event) => this.onClick('0')}>0</button>
            <button onClick ={(event) => this.onClick('/')}>/</button>
            <button onClick ={(event) => this.onClick('=')}>=</button>
          </div>
        </div>
      </div>
    )
  }
}

export default Calculator
